package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Book;
import play.core.j.HttpExecutionContext;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.BookService;
import services.DefaultBookService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class BookController extends Controller {
    private final BookService bookService;


    @Inject
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    public CompletionStage<Result> books(String q) {
        return bookService.get().thenApplyAsync(bookStream -> ok(Json.toJson(bookStream.collect(Collectors.toList()))));
    }


    public CompletionStage<Result> get(long id) {
        return bookService.get(id).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonBook = request.body().asJson();
        Book newBook = Json.fromJson(jsonBook, Book.class);
        return bookService.add(newBook).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> update(Http.Request request, long id) {
        JsonNode jsonBook = request.body().asJson();
        Book updatedBook = Json.fromJson(jsonBook, Book.class);
        updatedBook.setId(id);
        return bookService.update(updatedBook).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> delete(long id) {
        return bookService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }
}
