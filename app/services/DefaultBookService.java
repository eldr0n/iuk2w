package services;

import models.Book;
import models.BookRepository;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultBookService implements BookService {

    private BookRepository bookRepository;

    @Inject
    public DefaultBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public CompletionStage<Stream<Book>> get() {
        return bookRepository.list();
    }

    public CompletionStage<Book> get(Long id) {
        return bookRepository.find(id);
    }

    public CompletionStage<Boolean> delete(Long id) {
        return bookRepository.remove(id);
    }

    public CompletionStage<Book> update(Book book) {
        return bookRepository.update(book);
    }

    public CompletionStage<Book> add(Book book) {
        return bookRepository.add(book);
    }

}


