import React from "react";

class CreateBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        alert(this.state.title)
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Title:
                    <input type="text" name="title" value={this.state.title}
                    onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Create Book"/>
            </form>
        );
    }
}

export default CreateBook;