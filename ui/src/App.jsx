import React from 'react';
import './App.css';
import logo from "./book.svg"
import Books from "./book/books.jsx"
import CreateBook from "./book/CreateBook.jsx";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';


export default function App() {
    return (
        <div className="App">
            <Router>
                <header className="App">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>Welcome to our Book store</p>
                    <nav>
                        <Link to="/">Home</Link> |
                        <Link to="/books">Books</Link> |
                        <Link to="/create">Create Book</Link>
                    </nav>
                </header>
                <Switch>
                    <Route path="/books">
                        <Books/>
                    </Route>
                    <Route path="/create">
                        <CreateBook/>
                    </Route>
                    <Route path="/">
                        <p>Home componente erzeugen</p>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}
